#!pyobjects

def record(host, type, value):
    BotoRoute53.present(
        "%s_%s" % (host, type),
        name=host,
        value=value,
        zone="astro73.com",
        record_type=type,
        ttl=(5*60),
    )


def alias(target, source):
    ip4 = salt.dnsutil.A(source)
    if ip4:
        record(target, "A", ip4)

    ip6 = salt.dnsutil.AAAA(source)
    if ip6:
        record(target, "AAAA", ip6)


with BotoRoute53.hosted_zone_present(
    "astro73.com.",
    domain_name="astro73.com.",
    comment="",
):
    alias("astro73.com", "astronouth7303.github.io")
    alias("www.astro73.com", "astronouth7303.github.io")

    record("astro73.com", "MX", [
        "1 ASPMX.L.GOOGLE.COM.",
        "5 ALT1.ASPMX.L.GOOGLE.COM.",
        "5 ALT2.ASPMX.L.GOOGLE.COM.",
        "10 ASPMX2.GOOGLEMAIL.COM.",
        "10 ASPMX3.GOOGLEMAIL.COM.",
    ])

    # TODO: Make these into minions and use the minion dyndns records
    # record("ftbil.astro73.com", "CNAME", "ec2-54-161-213-158.compute-1.amazonaws.com.")
    # record("vanilla.astro73.com", "CNAME", "ec2-54-145-227-207.compute-1.amazonaws.com.")

    record("mail.astro73.com", "CNAME", "ghs.google.com.")

    record("astro73.com", "TXT", [
        '"v=spf1 include:aspmx.googlemail.com ~all"',
        '"astronouth7303-vhost.dftba.net"',
        '"google-site-verification=3wnCoBxkVeZKIe8zC7DFUGwISq_nl6cwx3laKAhaqo0"',
        '"keybase-site-verification=yOsgsrQOhaBrIUTg4cY3PXKYHYg0qdTKj0oGVDEpsyo"',
    ])